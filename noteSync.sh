#!/bin/bash
echo "Running Boostnote configuration..."
rclone config
echo "Running notes sync dry run ..."
rclone sync --dry-run /home/rodgers/Boostnote/ gDrive:Boostnote
while true
do
	echo "Do you still wish to sync? Yes [y] No [n]"
	read -p "Response: " reply
	echo $reply
	if [ $reply == "y" ]
	then
		echo "Running notes sync ..."
		rclone sync /home/rodgers/Boostnote/ gDrive:Boostnote
		echo "Notes updated succesfully :)"
		break
	elif [ $reply == "n" ]
	then
		echo "Exiting..."
		break
	else
		echo "Invalid reponse, try again!!"
	fi
done


