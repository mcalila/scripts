#!/bin/sh
cd /tmp
echo "Downloading JDK-9"
wget https://www.dropbox.com/s/56v5wfffl45i6xw/jdk-9.0.4_linux-x64_bin.tar.gz?dl=1
mv jdk-9.0.4_linux-x64_bin.tar.gz?dl=1 jdk-9.0.4_linux-x64_bin.tar.gz

echo "Uncompress the tar archive"
tar -xvzf jdk-9.0.4_linux-x64_bin.tar.gz

echo "Move the JDK 9 directory to /usr/lib/jvm/"
sudo mkdir -p /usr/lib/jvm
sudo mv jdk-9.0.4/ /usr/lib/jvm

echo "Assign Oracle JDK-9 a priority of 1, setting it as the default"
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk-9.0.4/bin/java" 1
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk-9.0.4/bin/javac" 1
sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk-9.0.4/bin/javaws" 1
sudo update-alternatives --install "/usr/bin/keytool" "keytool" "/usr/lib/jvm/jdk-9.0.4/bin/keytool" 1

echo "Apply the correct file ownerships and permissions to the jdk executables"
sudo chmod a+x /usr/bin/java
sudo chmod a+x /usr/bin/javac
sudo chown -R root:root /usr/lib/jvm/jdk-9.0.4

echo "Assign default jdk number if multiple are installed"
sudo update-alternatives --config java
