#! /bin/sh
 
#change workdir to suit your system
WORKDIR=/home/rodgers/.temp/
RNAME=gnome-shell-theme.gresource
GST=/usr/share/gnome-shell/$RNAME
MANIFEST=$RNAME.xml
mkdir -p $WORKDIR
cd $WORKDIR

echo '<?xml version="1.0" encoding="UTF-8"?>\n<gresources>\n<gresource prefix="/org/gnome/shell/theme">' > $WORKDIR/$MANIFEST

for file in `gresource list $GST`; do
	BASE=$(basename $file)
	OUT=$WORKDIR/$BASE
	gresource extract $GST $file > $OUT
	echo "<file>$BASE</file>" >> $WORKDIR/$MANIFEST
done

echo '</gresource>\n</gresources>' >> $WORKDIR/$MANIFEST