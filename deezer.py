#!/usr/local/bin/python3
"""
Download and decrypt songs from deezer.
The song is saved as a mp3.
No ID3 tags are added to the file.
The filename contains albom, artist, song title.
Usage:
python deezer-download.py  http://www.deezer.com/artist/12345
This will create mp3's in the current directory, with the song information in the filenames.
"""

import sys
from Crypto.Hash import MD5
from Crypto.Cipher import AES, Blowfish
import re
import os
import json
import struct
import urllib.request
import html.parser
from binascii import a2b_hex, b2a_hex

# global variable
host_stream_cdn = "http://e-cdn-proxy-%s.deezer.com/mobile/1"
setting_domain_img = "http://cdn-images.deezer.com/images"


def enabletor():
    import socks
    import socket

    def create_connection(address, timeout=None, source_address=None):
        sock = socks.socksocket()
        sock.connect(address)
        return sock

    socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)

    # patch the socket module
    socket.socket = socks.socksocket
    socket.create_connection = create_connection

    # todo: test if tor connection really works by connecting to https://check.torproject.org/


class ScriptExtractor(html.parser.HTMLParser):
    """ extract <script> tag contents from a html page """

    def __init__(self):
        super(ScriptExtractor, self).__init__()
        self.scripts = []
        self.curtag = None

    def handle_starttag(self, tag, attrs):
        self.curtag = tag.lower()

    def handle_data(self, data):
        if self.curtag == "script":
            self.scripts.append(data)

    def handle_endtag(self, tag):
        self.curtag = None


def find_re(txt, regex):
    """ Return either the first regex group, or the entire match """
    m = re.search(regex, txt)
    if not m:
        return
    gr = m.groups()
    if gr:
        return gr[0]
    return m.group()


def find_songs(obj):
    """ recurse into json object, yielding all objects which look like song descriptions """
    if type(obj) == list:
        for item in obj:
            yield from find_songs(item)
    elif type(obj) == dict:
        if "SNG_ID" in obj:
            yield obj
        for v in obj.values():
            yield from find_songs(v)


def parse_deezer_page(url):
    """
    extracts download host, and yields any songs found in a page.
    """
    f = urllib.request.urlopen(url)
    data = f.read()

    parser = ScriptExtractor()
    parser.feed(data.decode('utf-8'))
    parser.close()

    # note: keeping track of songs by songid, this might still lead to duplicate downloads,
    # for instance when the same song is present on multiple albums.
    # if you want to avoid that, add the MD5_ORIGIN to the foundsongs set, instead of the SNG_ID.
    foundsongs = set()
    for script in parser.scripts:
        var = find_re(script, r'var HOST_STREAM_CDN\s*=\s*\'(.*?)\';')
        if var:
            global host_stream_cdn
            host_stream_cdn = var.replace("{0}", "%s")

        var = find_re(script, r'var SETTING_DOMAIN_IMG\s*=\s*\'(.*?)\';')
        if var:
            global setting_domain_img
            setting_domain_img = var

        jsondata = find_re(script, r'\{.*MD5_ORIGIN.*\}')
        if jsondata:
            for song in find_songs(json.loads(jsondata)):
                if song["SNG_ID"] not in foundsongs:
                    yield song
                    foundsongs.add(song["SNG_ID"])


def md5hex(data):
    """ return hex string of md5 of the given string """
    h = MD5.new()
    h.update(data)
    return b2a_hex(h.digest())


def hexaescrypt(data, key):
    """ returns hex string of aes encrypted data """
    c = AES.new(key, AES.MODE_ECB)
    return b2a_hex(c.encrypt(data))


def genurlkey(songid, md5origin, mediaver=4, fmt=1):
    """ Calculate the deezer download url given the songid, origin and media+format """
    data = b'\xa4'.join(_.encode("utf-8")
                        for _ in [md5origin, str(fmt), str(songid), str(mediaver)])
    data = b'\xa4'.join([md5hex(data), data]) + b'\xa4'
    if len(data) % 16:
        data += b'\x00' * (16 - len(data) % 16)
    return hexaescrypt(data, "jo6aey6haid2Teih").decode('utf-8')


def calcbfkey(songid):
    """ Calculate the Blowfish decrypt key for a given songid """
    h = md5hex(b"%d" % songid)
    key = b"g4el58wc0zvf9na1"
    return "".join(chr(h[i] ^ h[i + 16] ^ key[i]) for i in range(16))


def blowfishDecrypt(data, key):
    """ CBC decrypt data with key """
    c = Blowfish.new(key, Blowfish.MODE_CBC, a2b_hex("0001020304050607"))
    return c.decrypt(data)


def decryptfile(fh, key, fo):
    """
    Decrypt data from file <fh>, and write to file <fo>.
    decrypt using blowfish with <key>.
    Only every third 2048 byte block is encrypted.
    """
    i = 0
    while True:
        data = fh.read(2048)
        if not data:
            break

        if (i % 3) == 0 and len(data) == 2048:
            data = blowfishDecrypt(data, key)
        fo.write(data)
        i += 1


def getformat(song):
    """ return format id for a song """
    if song["FILESIZE_MP3_320"]:
        return 3
    if song["FILESIZE_MP3_256"]:
        return 5
    return 1


def writeid3v1(fo, song):
    data = struct.pack("3s" "30s" "30s" "30s" "4s" "28s" "BB" "B",
                       b"TAG",
                       song["SNG_TITLE"].encode('utf-8'),
                       song["ART_NAME"].encode('utf-8'),
                       song["ALB_TITLE"].encode('utf-8'),
                       b"",   # year
                       b"",   # comment
                       0, int(song["TRACK_NUMBER"] or 0),  # tracknum
                       255)    # genre
    fo.write(data)


def downloadpicture(id):
    fh = urllib.request.urlopen(setting_domain_img + "/cover/" + id + "/200x200.jpg")
    return fh.read()


def writeid3v2(fo, song):
    def make28bit(x):
        return ((x << 3) & 0x7F000000) | ((x << 2) & 0x7F0000) | ((x << 1) & 0x7F00) | (x & 127)

    def maketag(tag, content):
        return struct.pack(">4sLH", bytes(tag, "ascii"), len(content), 0) + content

    def makeutf8(txt):
        return b"\x03" + (txt.encode('utf-8'))

    def makepic(data):
        imgframe = (b"\x00",          # text encoding
                    b"image/jpeg\x00",     # mime type
                    b"\x00",               # picture type: 'other'
                    b"\x00",               # description
                    data)
        return b''.join(imgframe)

    id3 = [
        maketag("TRCK", makeutf8("%02s" % str(song["TRACK_NUMBER"]))),  # decimal, no term NUL
        maketag("TIT2", makeutf8(song["SNG_TITLE"])),     # tern NUL ?
        maketag("TPE1", makeutf8(song["ART_NAME"])),      # tern NUL ?
        maketag("TALB", makeutf8(song["ALB_TITLE"])),     # tern NUL ?
    ]
    try:
        id3.append(maketag("APIC", makepic(downloadpicture(song["ALB_PICTURE"]))))
    except Exception as e:
        print("no pic", e)

    id3data = b"".join(id3)

    hdr = struct.pack(">3s" "H" "B" "L",
                      bytes("ID3", "ascii"),
                      0x400,   # version
                      0x00,    # flags
                      make28bit(len(id3data)))

    fo.write(hdr)
    fo.write(id3data)


def download(args, song):
    """ download and save a song to a local file, given the json dict describing the song """
    urlkey = genurlkey(int(song["SNG_ID"]), str(song["MD5_ORIGIN"]),
                       int(song["MEDIA_VERSION"]), getformat(song))
    key = calcbfkey(int(song["SNG_ID"]))

    tracknum = ("%2s-" % str(song["TRACK_NUMBER"])) if "TRACK_NUMBER" in song else ""

    outname = "%s - %s - %s%s - %010d.mp3" % (str(song["ART_NAME"]), str(
        song["ALB_TITLE"]), tracknum, str(song["SNG_TITLE"]), int(song["SNG_ID"]))
    outname = outname.replace('/', ',')
    if not args.overwrite and os.path.exists(outname):
        print("already there: %s" % outname)
        return
    try:
        fh = urllib.request.urlopen((host_stream_cdn + "/%s") %
                                    (str(song["MD5_ORIGIN"])[0], urlkey))
        with open(outname, "wb") as fo:
            print(outname)
            writeid3v2(fo, song)
            decryptfile(fh, key, fo)
            writeid3v1(fo, song)
    except Exception as e:
        print("ERROR downloading from %s: %s" % (host_stream_cdn, e))
        raise


def printinfo(song):
    """ print info for a song """
    print(
        "%9s %s %-5s %-30s %-30s %s" %
        (song["SNG_ID"],
         song["MD5_ORIGIN"],
         song["MEDIA_VERSION"],
         song["ART_NAME"],
         song["ALB_TITLE"],
         song["SNG_TITLE"]))


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Deezer downloader')
    parser.add_argument('--tor', '-T', action='store_true', help='Download via tor')
    parser.add_argument('--list', '-l', action='store_true', help='Only list songs found on page')
    parser.add_argument(
        '--overwrite',
        '-f',
        action='store_true',
        help='Overwrite existing downloads')
    parser.add_argument('urls', nargs='*', type=str)
    args = parser.parse_args()

    if args.tor:
        enabletor()

    for url in args.urls:
        for song in parse_deezer_page(url):
            if args.list:
                printinfo(song)
            else:
                print("...", song)
                downloaded = False
                try:
                    download(args, song)
                    downloaded = True
                except:
                    pass
                if not downloaded and "FALLBACK" in song:
                    try:
                        print("trying fallback")
                        download(args, song["FALLBACK"])
                        downloaded = True
                    except:
                        pass


if __name__ == '__main__':
    sys.exit(main())
