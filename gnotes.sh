#!/bin/bash
#echo "Running rclone configuration..."
#rclone config
echo "Running notes sync dry run ..."
rclone sync --dry-run /home/rodgers/.local/share/gnote Gnotes:Gnotes
while true
do
	echo "Do you still wish to sync? Yes [y] No [n]"
	read -p "Response: " reply
	echo $reply
	if [ $reply == "y" ]
	then
		echo "Running notes sync ..."
		rclone sync /home/rodgers/.local/share/gnote Gnotes:Gnotes
		echo "Notes updated succesfully :)"
		break
	elif [ $reply == "n" ]
	then
		echo "Exiting..."
		break
	else
		echo "Invalid reponse, try again!!"
	fi
done


