#!/bin/sh
cd /tmp
echo "Downloading JDK-8"
wget https://www.dropbox.com/s/70gq5hnsg2r7k1q/jdk-8u181-linux-x64.tar.gz?dl=1
mv jdk-8u181-linux-x64.tar.gz?dl=1 jdk-8u181-linux-x64.tar.gz

echo "Uncompress the tar archive"
tar -xvzf jdk-8u181-linux-x64.tar.gz 

echo "Move the JDK 8 directory to /usr/lib/jvm/"
sudo mkdir -p /usr/lib/jvm
sudo mv jdk1.8.0_181/ /usr/lib/jvm

echo "Assign Oracle JDK-8 a priority of 1, setting it as the default"
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_181/bin/java" 1
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_181/bin/javac" 1
sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk1.8.0_181/bin/javaws" 1
sudo update-alternatives --install "/usr/bin/keytool" "keytool" "/usr/lib/jvm/jdk1.8.0_181/bin/keytool" 1

echo "Apply the correct file ownerships and permissions to the jdk executables"
sudo chmod a+x /usr/bin/java
sudo chmod a+x /usr/bin/javac
sudo chown -R root:root /usr/lib/jvm/jdk1.8.0_181

echo "Assign default jdk number if multiple are installed"
sudo update-alternatives --config java
